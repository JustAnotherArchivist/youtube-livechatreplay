A method to grab the live chat replay from YouTube

* Requires qwarc and realpath from GNU coreutils 8.23+.
* Execute as `livechatreplay VIDEOID` where `VIDEOID` is the 11-character video ID from YouTube.
* You can pass multiple video IDs at once as well: `livechatreplay VIDEOID1 VIDEOID2 ...`. They get executed sequentially.
* All live chats (i.e. "top" and "all") are grabbed.
